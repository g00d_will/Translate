#!/bin/sh
# 此脚本用于提取crash文件中的UUID
# $1 crash文件名
# $2 项目真实名字
grep --after-context=5 "Binary Images:" "$1"  | grep $2