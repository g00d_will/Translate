#!/bin/sh
# 此脚本用于提取crash文件中的crash内存地址
# $1 项目真实名字
# $2 crash文件名
grep $1 "$2" | grep "0x00";