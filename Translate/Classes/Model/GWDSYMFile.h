//
//  GWDSYMFile.h
//  Translate
//
//  Created by will on 14/12/20.
//  Copyright (c) 2014年 Pinguo. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  dSYM文件模型
 */
@interface GWDSYMFile : NSObject

/** 未经处理的UUID串 */
@property (nonatomic, copy, readonly) NSString *srcUUID;

/** UUID  key是指令集 value是uuid字符串*/
@property (nonatomic, copy, readonly) NSDictionary *uuidDict;

/** 指令集 */
@property (nonatomic, strong, readonly) NSArray *archs;

/** 真实的项目名称 */
@property (nonatomic, copy, readonly) NSString *trueName;

- (instancetype)initWithSrcUUID:(NSString *)srcUUID;

@end
