//
//  GWFiles.h
//  Translate
//
//  Created by will on 14/12/19.
//  Copyright (c) 2014年 Pinguo. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  传入的文件模型
 */
@interface GWFiles : NSObject
/** crash文件的路径 */
@property (nonatomic, strong) NSArray *crashFilePaths;

/**  dSYMPath文件的路径 */
@property (nonatomic, strong) NSArray *dSYMFilePaths;

@end
