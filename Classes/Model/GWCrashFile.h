//
//  GWCrashFile.h
//  Translate
//
//  Created by will on 14/12/19.
//  Copyright (c) 2014年 Pinguo. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 *  crash文件模型
 */
@interface GWCrashFile : NSObject
/** crash文件的路径 */
@property (nonatomic, copy) NSString *path;

/**  crash文件的UUID */
@property (nonatomic, copy) NSString *uuid;

/** 真实的项目名称 */
@property (nonatomic, copy) NSString *trueName;

/** crash文件的文本内容 */
@property (nonatomic, copy) NSString *content;

/** 架构  (指令集armv7、armv7s、armv64) */
@property (nonatomic, copy) NSString *arch;

/**  是否匹配dsym文件 */
@property (nonatomic, assign) BOOL isMatchDsymUUID;

/** 纯内存地址作为Key，每一行文本作为valure */
@property (nonatomic, strong) NSDictionary *memoryDictSrc;

@end
