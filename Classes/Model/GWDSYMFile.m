//
//  GWDSYMFile.m
//  Translate
//
//  Created by will on 14/12/20.
//  Copyright (c) 2014年 Pinguo. All rights reserved.
//

#import "GWDSYMFile.h"
#import "RegexKitLite.h"

@implementation GWDSYMFile

- (instancetype)initWithSrcUUID:(NSString *)srcUUID;
{
    self = [super init];
    if (self)
    {
// 原始的：UUID: F26A496D-939F-3A20-8B08-B6D4287D706E (armv7) Camera360.app.dSYM/Contents/Resources/DWARF/Camera360
        _srcUUID = srcUUID;
        
        NSArray *array = [srcUUID componentsSeparatedByString:@"\n"];
        
        NSMutableDictionary *uuidDict = [NSMutableDictionary dictionary];
        for (NSString *uuidLong in array)
        {
            if (uuidLong.length > 0)
            {
                NSArray *uuidSeparates = [uuidLong componentsSeparatedByString:@" "];
                NSAssert(uuidSeparates.count == 4, @"dsym文件解析UUID失败");
                
                if (uuidSeparates.count == 4)
                {
                    // 1.获取UUID
                    NSString *uuid = uuidSeparates[1];
                    // 分片后，数组的的第二个位置是UUID，去掉 - 号，全部小写
                    uuid = [[uuid stringByReplacingOccurrencesOfString:@"-" withString:@""] lowercaseString];
                    
                    // 2.获取指令集
                    NSString *arch = uuidSeparates[2];
                    arch = [arch substringWithRange:NSMakeRange(1, arch.length - 2)]; // 去掉括号
                    
                    // 添加进uuid字典
                    uuidDict[arch] = uuid;
                    
                    // 3.获取项目真实名字
                    NSString *trueName = [uuidSeparates lastObject];
                    NSArray *trueArr = [uuidLong componentsSeparatedByString:@"/"];
                    trueName = [[trueArr lastObject] stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                    _trueName = trueName;
                }
                
            }
        }
        _uuidDict = uuidDict;
        _archs = uuidDict.allKeys;
        
    }
    return self;
}
@end
