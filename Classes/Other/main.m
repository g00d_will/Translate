//
//  main.m
//  Translate
//
//  Created by will on 14/12/24.
//  Copyright (c) 2014年 Pinguo. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
