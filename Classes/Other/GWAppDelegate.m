//
//  AppDelegate.m
//  Translate
//
//  Created by will on 14/12/14.
//  Copyright (c) 2014年 Pinguo. All rights reserved.
//

#import "GWAppDelegate.h"

@interface GWAppDelegate ()

@end

@implementation GWAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    
}

- (void)applicationWillTerminate:(NSNotification *)aNotification
{
    // Insert code here to tear down your application
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender
{
    return YES;
}


@end
