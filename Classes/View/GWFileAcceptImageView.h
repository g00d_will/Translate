//
//  FileAcceptImageView.h
//  Translate
//
//  Created by will on 14/12/14.
//  Copyright (c) 2014年 Pinguo. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@protocol GWFileAcceptImageViewDelegate <NSObject>
@optional
- (void)dragDropViewFileList:(NSArray *)fileList;

@end

@interface GWFileAcceptImageView : NSImageView

@property (nonatomic, copy) NSString *filePath;
@property (nonatomic, assign) IBOutlet id <GWFileAcceptImageViewDelegate> delegate;

@end
