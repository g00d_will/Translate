//
//  ViewController.h
//  Translate
//
//  Created by will on 14/12/14.
//  Copyright (c) 2014年 Pinguo. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class  GWFileAcceptImageView;

@interface GWViewController : NSViewController
/**  用于接收拖拽进来的文件 */
@property (weak) IBOutlet GWFileAcceptImageView *fileAcceptImageView;


@end

