//
//  GWMetaDataTool.h
//  Translate
//
//  Created by will on 14/12/20.
//  Copyright (c) 2014年 Pinguo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GWSingleton.h"
@class GWCrashFile;
/**
 *  模型管理
 */
@interface GWMetaDataTool : NSObject
GWSingletonH(GWMetaDataTool)


- (GWCrashFile *)crashFileWithPath:(NSString *)path;

/**
 *  获取GWCrashFile模型数组
 *
 *  @param crashPaths crash文件的路径数组
 *
 *  @return GWCrashFile模型数组
 */
- (NSArray *)crashFilesWithPaths:(NSArray *)crashPaths;

@end
