//
//  GWExecuteTool.h
//  Translate
//
//  Created by will on 14/12/19.
//  Copyright (c) 2014年 Pinguo. All rights reserved.
//
#define kShellAtosPath [[NSBundle mainBundle] pathForResource:@"atos" ofType:@"sh"]
#define kShellDwarfdumpPath [[NSBundle mainBundle] pathForResource:@"dwarfdump" ofType:@"sh"]
#define kShellGrepCrashPath [[NSBundle mainBundle] pathForResource:@"grepcrashuuid" ofType:@"sh"]

#import <Foundation/Foundation.h>
#import "GWSingleton.h"
@class GWDSYMFile;
@class GWCrashFile;

/**
 *  执行脚本工具类
 */
@interface GWExecuteTool : NSObject
GWSingletonH(GWExecuteTool)

/**
 *  执行脚本方法
 *
 *  @param path    脚本路径
 *  @param param   脚本参数
 *  @param success 成功block
 *  @param failure 失败block
 */
+ (void)executeShellPath:(NSString *)path param:(NSArray *)params success:(void (^)(NSString *output))success failure:(void (^)(NSString *error))failure;

/**
 *  符号化指定的内存地址 （执行atos.sh脚本）
 *
 *  @param path    dSYM文件路径
 *  @param arch    dSYM文件的架构类型（armv7、armv7s、armv64）
 *  @param address 需要被符号化的内存地址
 */
+ (void)symbolMemoryWithDsymPath:(NSString *)dSYMpath arch:(NSString *)arch address:(NSArray *)address success:(void (^)(NSDictionary *symbolDict))success failure:(void (^)(NSString *error))failure;

/**
 *  模型化dSYM文件 （执行dwarfdump.sh脚本）
 *
 *  @param dSYMpath dSYM文件路径
 *  @param success  成功block
 *  @param failure  失败block
 */
+ (void)fetchDsymModelWithPath:(NSString *)dSYMpath success:(void (^)(GWDSYMFile *dSYM))success failure:(void (^)(NSString *error))failure;

/**
 *  模型化crash文件 （执行grepCrash.sh 脚本）
 *
 *  @param crashPath crash文件路径
 *  @param dsymPath  dSYM文件路径
 *  @param success   成功block
 *  @param failure   失败block
 */
+ (void)fetchCrashModelWithCrashPath:(NSString *)crashPath dSYMPath:(NSString *)dsymPath success:(void (^)(GWCrashFile *crash))success failure:(void (^)(NSString *error))failure;

+ (void)fetchCrashMemorysPath:(NSString *)path trueName:(NSString *)trueName success:(void (^)(NSDictionary *memorysDict))success failure:(void (^)(NSString *error))failure;
@end
