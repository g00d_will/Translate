//
//  TextTool.m
//  Translate
//
//  Created by will on 14/12/19.
//  Copyright (c) 2014年 Pinguo. All rights reserved.
//

#import "GWFileTool.h"
#import "RegexKitLite.h"
#import "GWExecuteTool.h"

@interface GWFileTool ()


@end

@implementation GWFileTool
GWSingletonM(GWFileTool)

+ (void)filePathsWithArray:(NSArray *)filePaths succes:(FilesPathsSuccessBlock)success failure:(FilesPathsFailureBlock)failure
{
    NSMutableArray *crashFilePaths = [NSMutableArray array];
    NSMutableArray *dsymFilepaths = [NSMutableArray array];

    for (NSString *fp in filePaths)
    {
        // 文件路径转换为纯小写，方便匹配后缀
        NSString *lowFilePath = [fp lowercaseString];
        
        if ([lowFilePath isMatchedByRegex:@"\\.crash$"])
        {
            // 如果是.crash 后缀
            [crashFilePaths addObject:fp];
        }
        else if ([lowFilePath isMatchedByRegex:@"\\.dsym$"])
        {
            // 如果是.dsym 后缀
            [dsymFilepaths addObject:fp];
        }

    }
    
    if (crashFilePaths.count > 0 && dsymFilepaths.count > 0 && success != nil)
    {
        GWFiles *gwF = [[GWFiles alloc] init];
        gwF.dSYMFilePaths = dsymFilepaths;
        gwF.crashFilePaths = crashFilePaths;
        
        // 成功的回调
        success(gwF);
    }
    else if (failure)
    {
        // 失败的回调
        if (crashFilePaths.count == 0) {
            failure(@"没有后缀名是crash的文件");
        }
        else if (dsymFilepaths.count == 0)
        {
            failure(@"没有后缀名是dSYM的文件");
        }
    }
    
}

@end
