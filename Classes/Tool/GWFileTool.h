//
//  TextTool.h
//  Translate
//
//  Created by will on 14/12/19.
//  Copyright (c) 2014年 Pinguo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GWSingleton.h"
#import "GWFiles.h"

typedef void (^FilesPathsSuccessBlock)(GWFiles *files);
typedef void (^FilesPathsFailureBlock)(NSString *error);
/**
 *  文件处理工具类
 */
@interface GWFileTool : NSObject

GWSingletonH(GWFileTool)

/**
 *  获取读取的文件模型，过滤crash文件和dSYM文件
 *
 *  @param filePaths 拖拽进来的所有文件路径
 *  @param success   成功block
 *  @param failure   失败block
 */
+ (void)filePathsWithArray:(NSArray *)filePaths succes:(FilesPathsSuccessBlock)success failure:(FilesPathsFailureBlock)failure;

/**
 *  dsym文件与crash文件的uuid是否匹配
 *
 *  @param dsymPath  dsym文件的路径
 *  @param crashPath crash文件的路径
 *
 *  @return 匹配返回YES，不匹配返回NO
 */

@end
